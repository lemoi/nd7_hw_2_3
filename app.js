const express = require('express');
const bodyParser = require("body-parser");

const app = express();
const PORT = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({"extended": true}));

app.get('/', (req, res) => {
	res.status(200).send('Hello, Express.js');
});

app.get('/hello', (req, res) => {
	res.status(200).send('Hello stranger!');
});

app.get('/hello/:name', (req, res) => {
	res.status(200).send(`Hello, ${req.params.name}`);
});

app.all('/sub/*', (req, res) => {
	res.send(`You requested URI: ${req.url}`);
});

const middleware = function (req, res, next) {
    if(!req.headers.key) {
        //next(new Error(401));
        return res.sendStatus(401);
    }
    next();
};

app.post('/post', middleware, (req, res) => {
	if(!req.body) {
		return res.sendStatus(404);
	};
	return res.json(req.body);
});

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));